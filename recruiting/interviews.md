# Interviews

[Home](../index.md)



When designing interviews, keep the following in mind:

1. Structrural
   1. Take aggressive steps to mitigate bias
      1. Panelists should not "invent" their interview questions on the fly, this isn't a test of the panelist's ability to think on their feet
      2. Panelists should collaboratively create a question bank for an open position to standardize questions
      3. Panelist performance should be assessed (i.e who is good at identifiyng great talent in an interview?)
      4. All candidates interviewing for a specific open position should all be asked questions from the same (limited) question bank, and be assessed along the same axes
      5. Panelists should minimize the amount of in-interview analysis; instead take copious notes and analyze immediately after the interview.
      6. Here's a [clonable gdoc template](https://docs.google.com/document/d/1TPYh8y4Xr4JC2x0R15CF3fHwiCB_l06ln7z7Wq-P0bM/edit?usp=sharing) that uses these principles.
   2. Implement a formal process to track panelist (judement) performance
      1. Metrics
         1. What percentage of candidates hired by a given panelist perform well in their first performance review?
2. Operational
   1. Interview duration should be predictable, ideally no more than 60 minutes.
   2. Templates, templates, templates - they save time, and help standardise
      1. [Interview Notes Template]((https://docs.google.com/document/d/1TPYh8y4Xr4JC2x0R15CF3fHwiCB_l06ln7z7Wq-P0bM/edit?usp=sharing)) for panelists to use
      2. JD Templates for hiring managers to use
         1. The JD should clearly explain how the performance of someone hired for that position will be evaluated. What are you hiring to solve for?
   3. Implement a formal process to deliver feedback to rejected candidates weekly.
      1. Metrics
         1. %age of rejected candidates reporting no feedback
   4. Implement a formal process to track panelist (operational) performance
         1. %age of rejected candidates who say they will recommend you to friends and colleagues 
         2. Metrics
         3. Number of no-shows, (i.e no heads up given, panelist just doesn't show
         4. Number of reschedules with notification
         5. etc.

## Reading

1. Thinking Fast and Slow, Kahneman
